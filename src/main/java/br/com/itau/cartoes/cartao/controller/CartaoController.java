package br.com.itau.cartoes.cartao.controller;

import br.com.itau.cartoes.cartao.model.Cartao;
import br.com.itau.cartoes.cartao.model.dto.*;
import br.com.itau.cartoes.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapDTO cartaoMapDTO;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AtualizarCartaoResponseDTO criar(@Valid @RequestBody CartaoDTO cartaoDTO) {
        Cartao cartao = cartaoMapDTO.converterParaCartao(cartaoDTO);

        cartao = cartaoService.criar(cartao);

        return cartaoMapDTO.converterParaAtualizarCartaoResponseDTO(cartao);
    }

    @PatchMapping("/{numero}")
    public AtualizarCartaoResponseDTO atualizar(@PathVariable String numero, @RequestBody EditarCartaoDTO editarCartaoDTO) {
        editarCartaoDTO.setNumero(numero);
        Cartao cartao = cartaoMapDTO.converterParaCartao(editarCartaoDTO);

        cartao = cartaoService.atualizar(cartao);

        return cartaoMapDTO.converterParaAtualizarCartaoResponseDTO(cartao);
    }

    @GetMapping("/{numero}")
    public CartaoDTO getByNumber(@PathVariable String numero) {
        Cartao cartao = cartaoService.buscarPorNumero(numero);
        return cartaoMapDTO.converterParaCartaoDTO(cartao);
    }
}
