package br.com.itau.cartoes.cartao.service;

import br.com.itau.cartoes.cartao.exceptions.CartaoNaoEncontradoException;
import br.com.itau.cartoes.cartao.repository.CartaoRepository;
import br.com.itau.cartoes.cliente.models.Cliente;
import br.com.itau.cartoes.cliente.service.ClienteService;
import br.com.itau.cartoes.cartao.model.Cartao;
import br.com.itau.cartoes.fatura.model.Fatura;
import br.com.itau.cartoes.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private FaturaService faturaService;

    public Cartao criar(Cartao cartao) {
        Cliente cliente = clienteService.getById(cartao.getCliente().getId());
        cartao.setCliente(cliente);
        cartao.setAtivo(false);

        cartao = cartaoRepository.save(cartao);

        List<Fatura> faturas = new ArrayList<>();
        faturas.add(faturaService.criarNovaFatura(cartao));
        cartao.setFaturas(faturas);

        return cartaoRepository.save(cartao);
    }

    public Cartao atualizar(Cartao cartao) {
        Cartao cartaoDB = buscarPorNumero(cartao.getNumero());

        cartaoDB.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(cartaoDB);
    }

    public Cartao buscarPorNumero(String numero) {

        Cartao cartao = cartaoRepository.findByNumero(numero)
                .orElseThrow(CartaoNaoEncontradoException::new);

        return cartao;
    }

    public Cartao buscarPorId(Long id) {
        Cartao cartao = cartaoRepository.findById(id)
                .orElseThrow(CartaoNaoEncontradoException::new);

        return cartao;
    }

}
