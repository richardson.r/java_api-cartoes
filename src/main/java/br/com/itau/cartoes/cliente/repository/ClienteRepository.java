package br.com.itau.cartoes.cliente.repository;

import br.com.itau.cartoes.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
