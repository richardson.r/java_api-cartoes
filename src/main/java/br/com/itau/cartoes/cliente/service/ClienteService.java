package br.com.itau.cartoes.cliente.service;

import br.com.itau.cartoes.cliente.exceptions.ClienteNaoEncontradoException;
import br.com.itau.cartoes.cliente.models.Cliente;
import br.com.itau.cartoes.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente getById(Long id) {
        Cliente cliente = clienteRepository.findById(id)
                .orElseThrow(ClienteNaoEncontradoException::new);

        return cliente;
    }

}
