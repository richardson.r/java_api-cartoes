package br.com.itau.cartoes.fatura.service;

import br.com.itau.cartoes.cartao.model.Cartao;
import br.com.itau.cartoes.cartao.service.CartaoService;
import br.com.itau.cartoes.fatura.exceptions.FaturaNaoEncontradaException;
import br.com.itau.cartoes.fatura.model.Fatura;
import br.com.itau.cartoes.fatura.repository.FaturaRepository;
import br.com.itau.cartoes.pagamento.models.Pagamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private CartaoService cartaoService;

    public Fatura salvar(Fatura fatura){
        return faturaRepository.save(fatura);
    }

    public Fatura listarFatura(Long cartaoId, int mes, int ano){
        Fatura fatura = faturaRepository.findByCartaoIdAndMesAndAno(cartaoId, mes, ano)
                .orElseThrow(FaturaNaoEncontradaException::new);

        return fatura;
    }

    public Fatura listarFaturaAberta(Cartao cartao){
        Optional<Fatura> faturaOptional = faturaRepository.findByCartaoIdAndAberta(cartao.getId(), true);

        if(faturaOptional.isPresent()){
            return faturaOptional.get();
        }else {
            return criarNovaFatura(cartaoService.buscarPorId(cartao.getId()));
        }
    }

    public Fatura listarFaturaAberta(Long cartaoId){
        Optional<Fatura> faturaOptional = faturaRepository.findByCartaoIdAndAberta(cartaoId, true);

        if(faturaOptional.isPresent()){
            return faturaOptional.get();
        }else {
            return criarNovaFatura(cartaoService.buscarPorId(cartaoId));
        }
    }

    public Fatura criarNovaFatura(Cartao cartao){
        Fatura fatura = new Fatura();
        fatura.setCartaoId(cartao.getId());
        fatura.setAno(LocalDate.now().getYear());
        fatura.setMes(LocalDate.now().getMonthValue());
        fatura.setAberta(true);

        return salvar(fatura);
    }

    public void adicionarPagamento(Long cartaoId, Pagamento pagamento){
        Fatura fatura = new Fatura();
        fatura = listarFaturaAberta(cartaoId);

        fatura.getPagamentos().add(pagamento);
        salvar(fatura);
    }
}
