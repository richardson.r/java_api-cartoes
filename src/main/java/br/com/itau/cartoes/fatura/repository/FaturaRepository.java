package br.com.itau.cartoes.fatura.repository;

import br.com.itau.cartoes.fatura.model.Fatura;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FaturaRepository extends CrudRepository<Fatura, Long> {
    List<Fatura> findAllByCartaoId(Long cartaoId);

    Optional<Fatura> findByCartaoIdAndMesAndAno(Long cartaoId, int mes, int ano);

    Optional<Fatura> findByCartaoIdAndAberta(Long cartaoId, boolean aberta);
}
