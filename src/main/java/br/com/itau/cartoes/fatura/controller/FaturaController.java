package br.com.itau.cartoes.fatura.controller;

import br.com.itau.cartoes.fatura.model.Fatura;
import br.com.itau.cartoes.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @GetMapping("/{clienteId}/{cartaoId}")
    public Fatura exibirFaturaAberta(@PathVariable Long clienteId, @PathVariable Long cartaoId) {
        Fatura fatura = faturaService.listarFaturaAberta(cartaoId);
        return fatura;
    }

    @PostMapping("/{clienteId}/{cartaoId}/pagar")
    @ResponseStatus(HttpStatus.CREATED)
    public Fatura pagar(@RequestBody Fatura fatura) {
        Fatura faturaSalva = faturaService.salvar(fatura);

        return faturaSalva;
    }
}
