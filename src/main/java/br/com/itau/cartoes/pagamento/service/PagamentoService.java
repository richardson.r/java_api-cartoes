package br.com.itau.cartoes.pagamento.service;

import br.com.itau.cartoes.fatura.service.FaturaService;
import br.com.itau.cartoes.pagamento.models.Pagamento;
import br.com.itau.cartoes.pagamento.models.dto.PagamentoDTO;
import br.com.itau.cartoes.pagamento.models.dto.PagamentoMapDTO;
import br.com.itau.cartoes.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private PagamentoMapDTO pagamentoMapDTO;

    @Autowired
    private FaturaService faturaService;

    public PagamentoDTO criar(PagamentoDTO pagamentoDTO) {
        Pagamento pagamento = new Pagamento();

        pagamento = pagamentoMapDTO.converterParaPagamento(pagamentoDTO);
        pagamento = pagamentoRepository.save(pagamento);

        faturaService.adicionarPagamento(pagamento.getCartaoId(), pagamento);

        return pagamentoMapDTO.converterParaPagamentoDTO(pagamento);
    }

    public List<PagamentoDTO> listarPagamentosCartao(Long idCartao) {
        List<PagamentoDTO> pagamentosDTO = new ArrayList<>();
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(idCartao);

        for (Pagamento pagamento : pagamentos) {
            pagamentosDTO.add(pagamentoMapDTO.converterParaPagamentoDTO(pagamento));
        }

        return pagamentosDTO;
    }

}
