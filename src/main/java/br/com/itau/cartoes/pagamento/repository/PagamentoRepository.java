package br.com.itau.cartoes.pagamento.repository;

import br.com.itau.cartoes.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartaoId(Long idCartao);

}
