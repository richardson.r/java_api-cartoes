package br.com.itau.cartoes.pagamento.models.dto;

import br.com.itau.cartoes.fatura.service.FaturaService;
import br.com.itau.cartoes.pagamento.models.Pagamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapDTO {

    @Autowired
    private FaturaService faturaService;

    public Pagamento converterParaPagamento(PagamentoDTO pagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoDTO.getDescricao());
        pagamento.setValor(pagamentoDTO.getValor());

        pagamento.setCartaoId(pagamentoDTO.getCartao_id());
        pagamento.setFaturaId(
                faturaService.listarFaturaAberta(
                        pagamentoDTO.getCartao_id()
                ).getId()
        );

        return pagamento;
    }

    public PagamentoDTO converterParaPagamentoDTO(Pagamento pagamento) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();
        pagamentoDTO.setId(pagamento.getId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());
        pagamentoDTO.setCartao_id(pagamento.getCartaoId());

        return pagamentoDTO;
    }
}




